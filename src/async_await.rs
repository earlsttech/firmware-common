// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

//! Heapless and atomic-less implementation of async execution intended to be
//! used on RTIC.

use core::{
    hint::spin_loop,
    marker::PhantomPinned,
    mem,
    ops::Sub,
    pin::Pin,
    ptr::{null, null_mut},
    sync::atomic::{AtomicBool, AtomicPtr, Ordering as AO},
    task::{Context, Poll, RawWaker, RawWakerVTable, Waker},
    time::Duration,
};
use embedded_hal::timer::CountDown;
use futures::{pin_mut, Future};
use rtic::{export::interrupt::Nr, pend, Monotonic};

/// A single-task executor which is capable of yielding control and supports
/// heapless operation.
pub struct YieldingExecutor<C: MonotonicComparable>
where
    <<C as Monotonic>::Instant as Sub>::Output: Ord,
{
    future: AtomicPtr<Pin<&'static mut (dyn Future<Output = !> + Send + Sync)>>,
    ready: AtomicBool,
    interrupt: FinalInterrupt,
    delay_chain: AtomicPtr<Delay<C>>,
}

// TODO: we need to use the RTIC scheduleing API instead of making our own.

impl<C: MonotonicComparable> YieldingExecutor<C>
where
    <<C as Monotonic>::Instant as Sub>::Output: Ord,
{
    /// Creates a new executor which will run on the given interrupt.
    pub fn new(interrupt: impl Nr) -> Self {
        Self {
            future: AtomicPtr::new(null_mut()),
            ready: AtomicBool::new(false),
            interrupt: FinalInterrupt::new(interrupt),
            delay_chain: AtomicPtr::new(null_mut()),
        }
    }

    /// Function to call when the interrupt for the executor is triggered.
    ///
    /// # Safety
    /// Calling this function must be the entirety of the interrupt handler for
    /// the attached interrupt, and that must be the only place where it is
    /// called.
    pub unsafe fn step<TIM: CountDown, T: Into<TIM::Time>>(
        &self,
        timer: &mut TIM,
        convert: impl FnOnce(<C::Instant as Sub>::Output) -> T,
    ) {
        const WAKER_VTABLE: RawWakerVTable =
            RawWakerVTable::new(waker_clone, waker_wake, waker_wake_by_ref, waker_drop);

        unsafe fn waker_clone(data: *const ()) -> RawWaker {
            RawWaker::new(data, &WAKER_VTABLE)
        }

        unsafe fn waker_wake(data: *const ()) {
            pend(FinalInterrupt(data as u8))
        }

        unsafe fn waker_wake_by_ref(data: *const ()) {
            pend(FinalInterrupt(data as u8))
        }

        unsafe fn waker_drop(_data: *const ()) {}

        if !self.ready.load(AO::Acquire) {
            return;
        }

        let future = self.future.load(AO::Relaxed);
        let waker = Waker::from_raw(RawWaker::new(
            self.interrupt.nr() as *const _,
            &WAKER_VTABLE,
        ));
        match (*future).as_mut().poll(&mut Context::from_waker(&waker)) {
            Poll::Ready(x) => match x {},
            Poll::Pending => {}
        }

        let delay = self.delay_chain.load(AO::Relaxed);
        if !delay.is_null() {
            let diff = (*delay).time - C::now();

            if diff > C::zero_diff() {
                timer.start(convert(diff))
            } else {
                pend(self.interrupt);
            }
        }
    }

    /// Dispatches the given future to the task and returns when it has
    /// completed.
    ///
    /// # Safety
    /// This should only be called once, and from a task of lower priority than
    /// that which runs the executor; usually, this would comprise the entirety
    /// of the `idle` task.
    pub unsafe fn init_heapless(&self, mut root: impl Future<Output = !> + Send + Sync) -> ! {
        let root: &mut (dyn Future<Output = !> + Send + Sync) = &mut root;
        let mut root_pin: Pin<&mut (dyn Future<Output = !> + Send + Sync)> =
            Pin::new_unchecked(mem::transmute_copy(&root));
        let root_ptr = &mut root_pin as *mut _;

        self.future.store(root_ptr, AO::Relaxed);
        self.ready.store(true, AO::Release);

        pend(self.interrupt);

        loop {
            spin_loop()
        }
    }

    /// Creates a future which resolves after the given duration has passed.
    pub fn delay(&self, time: Duration) -> impl Future<Output = ()>
    where
        C: RealTime,
    {
        self.delay_until(C::offset(C::now(), time))
    }

    #[inline]
    /// Creates a future which resolves at the given instant.
    pub fn delay_until(&self, time: C::Instant) -> impl Future<Output = ()> {
        Delay::<C> {
            time,
            exec: self,
            prev_ptr: AtomicPtr::new(null_mut()),
            next: AtomicPtr::new(null_mut()),
            _phantom: PhantomPinned,
        }
    }
}

struct Delay<C: MonotonicComparable>
where
    <<C as Monotonic>::Instant as Sub>::Output: Ord,
{
    time: C::Instant,
    exec: *const YieldingExecutor<C>,
    prev_ptr: AtomicPtr<AtomicPtr<Delay<C>>>,
    next: AtomicPtr<Delay<C>>,
    _phantom: PhantomPinned,
}

impl<C: MonotonicComparable> Future for Delay<C>
where
    <<C as Monotonic>::Instant as Sub>::Output: Ord,
{
    type Output = ();

    fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
        let prev_ptr = self.prev_ptr.load(AO::Relaxed);

        if self.time - C::now() <= C::zero_diff() {
            if !prev_ptr.is_null() {
                unsafe {
                    (*prev_ptr).store(self.next.load(AO::Relaxed), AO::Relaxed);
                }
            }

            Poll::Ready(())
        } else {
            if prev_ptr.is_null() {
                let now = C::now();

                unsafe {
                    let mut ptr: *const AtomicPtr<Delay<C>> = &(*self.exec).delay_chain;
                    let mut cur = (*ptr).load(AO::Relaxed);

                    while !cur.is_null() && (*cur).time - now < self.time - now {
                        ptr = &(*cur).next;
                        cur = (*ptr).load(AO::Relaxed);
                    }

                    self.prev_ptr.store(mem::transmute(ptr), AO::Relaxed);
                    let self_ref = self.get_unchecked_mut();

                    if !cur.is_null() {
                        (*cur).prev_ptr.store(&mut self_ref.next, AO::Relaxed);
                        self_ref.next.store(cur, AO::Relaxed);
                    }

                    (*ptr).store(self_ref, AO::Relaxed);
                }
            }

            Poll::Pending
        }
    }
}

unsafe impl<C: MonotonicComparable> Send for Delay<C> where
    <<C as Monotonic>::Instant as Sub>::Output: Ord
{
}

unsafe impl<C: MonotonicComparable> Sync for Delay<C> where
    <<C as Monotonic>::Instant as Sub>::Output: Ord
{
}

#[derive(Clone, Copy)]
struct FinalInterrupt(u8);

unsafe impl Nr for FinalInterrupt {
    fn nr(&self) -> u8 {
        self.0
    }
}

impl FinalInterrupt {
    fn new(interrupt: impl Nr) -> Self {
        Self(interrupt.nr())
    }
}

/// Runs the given future synchronously.
pub fn run_sync<T>(root: impl Future<Output = T>) -> T {
    const WAKER_VTABLE: RawWakerVTable =
        RawWakerVTable::new(waker_clone, waker_wake, waker_wake_by_ref, waker_drop);

    unsafe fn waker_clone(data: *const ()) -> RawWaker {
        RawWaker::new(data, &WAKER_VTABLE)
    }

    unsafe fn waker_wake(_data: *const ()) {}

    unsafe fn waker_wake_by_ref(_data: *const ()) {}

    unsafe fn waker_drop(_data: *const ()) {}

    pin_mut!(root);
    let waker = unsafe { Waker::from_raw(RawWaker::new(null(), &WAKER_VTABLE)) };
    let cx = &mut Context::from_waker(&waker);
    loop {
        match root.as_mut().poll(cx) {
            Poll::Ready(r) => return r,
            Poll::Pending => {}
        }
    }
}

/// An extension to the [`Monotonic`] crate from RTIC which ensures that
/// durations are ordered.
pub trait MonotonicComparable: Monotonic
where
    <Self::Instant as Sub>::Output: Ord,
{
    /// The zero duration.
    fn zero_diff() -> <Self::Instant as Sub>::Output;
}

/// An extension to the [`Monotonic`] crate from RTIC which adds support for
/// applying realtime offsets to instants.
pub trait RealTime: Monotonic {
    /// Applies an offset to a given instant.
    fn offset(base: Self::Instant, offset: Duration) -> Self::Instant;
}

#[macro_export]
/// Generates an implementation of [`RealTime`] based on an RTIC `CYCCNT` or
/// compatible clock.
macro_rules! impl_cyccnt_rt {
    ($clock:ty, $name:ident, $num:literal / $den:literal) => {
        #[derive(PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
        pub struct Instant(<$clock as ::rtic::Monotonic>::Instant);

        impl ::core::ops::Add<i64> for Instant {
            type Output = Self;

            fn add(self, rhs: i64) -> Self {
                Instant(if rhs > 0 {
                    self.0 + (rhs as u32).cycles()
                } else {
                    self.0 - (-rhs as u32).cycles()
                })
            }
        }

        impl ::core::ops::Sub for Instant {
            type Output = i64;

            fn sub(self, rhs: Self) -> i64 {
                if self.0 > rhs.0 {
                    (self.0 - rhs.0).as_cycles() as i64
                } else {
                    -((rhs.0 - self.0).as_cycles() as i64)
                }
            }
        }

        #[allow(non_camel_case_types)]
        pub struct $name;

        impl ::rtic::Monotonic for $name {
            type Instant = Instant;

            fn ratio() -> ::rtic::Fraction {
                <$clock as ::rtic::Monotonic>::ratio()
            }

            fn now() -> Self::Instant {
                Instant(<$clock as ::rtic::Monotonic>::now())
            }

            unsafe fn reset() {
                <$clock as ::rtic::Monotonic>::reset()
            }

            fn zero() -> Self::Instant {
                Instant(<$clock as ::rtic::Monotonic>::zero())
            }
        }

        impl $crate::async_await::MonotonicComparable for $name {
            fn zero_diff() -> <Self::Instant as ::core::ops::Sub>::Output {
                0
            }
        }

        impl $crate::async_await::RealTime for $name {
            fn offset(base: Self::Instant, offset: ::core::time::Duration) -> Self::Instant {
                base + ((::core::time::Duration::as_nanos(&offset) * $den
                    / ($num * 1_000_000_000u128)) as i64)
            }
        }
    };
}
