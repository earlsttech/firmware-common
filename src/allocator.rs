// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

//! Interface to the Cortex M allocator. The symbols `_heap_start` and
//! `_heap_end` must be defined, likely by the memory layout linker script.

use alloc_cortex_m::CortexMHeap;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

/// Initializes the allocator.
///
/// # Safety
/// Must only be called once, and must be called before any allocations occur.
pub unsafe fn init() {
    ALLOCATOR.init(
        (&_heap_start as *const _) as usize,
        (&_heap_end as *const _) as usize - (&_heap_start as *const _) as usize,
    );
}

extern "C" {
    static _heap_start: u32;
    static _heap_end: u32;
}
