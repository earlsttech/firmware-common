// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

//! Library of common constructs and utilities that are not specific to any one board.

#![warn(missing_docs)]
#![no_std]
#![feature(maybe_uninit_ref)]
#![feature(never_type)]

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "allocator")]
pub mod allocator;

#[cfg(feature = "async")]
pub mod async_await;
